-- ICE

set foreign_key_checks = 0;
drop table RawMaterials;
drop table Cookies;
drop table Ingredients;
drop table Customers;
drop table Orders;
drop table Pallets;
set foreign_key_checks = 1;

-----------------------------

create table RawMaterials (
Name varchar(40),
Quantity int,
LastTransportTime DateTime default now() on update now(),
LastTransportQuantity int,
Unit varchar(10),
primary key (Name)
);

create table Cookies (
Name varchar(40),
IsBlocked bit(1),
primary key (Name)
);

create table Ingredients (
MaterialName varchar(40),
RecipeName varchar(40),
Amount int,
primary key (MaterialName, RecipeName),
foreign key (MaterialName) references RawMaterials (Name),
foreign key (RecipeName) references Cookies (Name)
);

create table Customers (
Name varchar(40),
Address varchar(100),
primary key (Name, Address)
);

create table Orders (
Id int auto_increment,
Destination varchar(100),
DeliveryDate Date,
TimeOrdered DateTime default now(),
CustomerName varchar(40),
primary key (Id),
foreign key (CustomerName, Destination) references Customers (Name, Address)
);


create table Pallets (
Id int auto_increment,
CookieName varchar(40),
Status int,
CurrentLocation varchar(40),
OrderId int,
ProductionDateTime DateTime default now(),
DeliveryDateTime DateTime,
IsBlocked bit(1),
primary key (Id),
foreign key (CookieName) references Cookies (Name),
foreign key (OrderId) references Orders (Id)
);


--- STATMENTS ---

-- At any time, we must be able to check how many pallets of a product that have been produced during a specific time.

select count(*) 
from Pallets
where ProductionDateTime between "2016-03-01 00:00:00" and "2016-03-01 23:59:59";

-- We must be able to check the amount in store of each ingredient, and to see when, and how much of, an ingredient was last delivered into storage.

select * 
from RawMaterials;

-- We must be able to trace each pallet.
select CurrentLocation
from Pallets
where Id = 1;

-- (the contents of the pallet, the location of the pallet, if the pallet is delivered and in that case to whom, etc.)

select CookieName, CurrentLocation, Status, Destination
from Pallets natural join Orders
where Pallets.Id = 1;

-- We must also be able to see which pallets that contain a certain product

select Id
from Pallets
where CookieName = "Nut Cookie";

-- We need to find out which products that are blocked, and also which pallets that contain a certain blocked product.

select CookieName
from Cookies
where IsBlocked = 1;

select Id, CookieName
from Pallets
where IsBlocked = 1;

-- Finally, we must be able to check which pallets that have been delivered to a given customer, and the date and time of delivery.

select Id, DeliveryDateTime
from Pallets
where status like "DELIVERED%";

-- we must have a facility to see all orders that are to be delivered during a specific time period.

select Id
from Orders
where DeliveryDate between "2016-03-01" and "2016-03-31";
