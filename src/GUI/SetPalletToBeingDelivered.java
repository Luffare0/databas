package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import cookies.Database;

public class SetPalletToBeingDelivered extends GeneralButton implements ActionListener{

	public SetPalletToBeingDelivered(Database db,
			JTable searchResult, JTextField searchField) {
		super("Set selected pallet(s) to being delivered", db, searchResult, searchField);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int[] selectedRows = searchResult.getSelectedRows();
		TableModel model = searchResult.getModel();
		for (int i = 0; i < selectedRows.length; ++i) {
			int selectedRow = selectedRows[i];
			int palletId = Integer.parseInt((String) model.getValueAt(selectedRow, 0));
			boolean result = db.setPalletToBeingDelivered(palletId);
			if (result) {
				model.setValueAt("being delivered", selectedRow, 2);
			} else {
				showErrorMessage("Could not set to being delivered");
			}
		}
		
	}

}
