package GUI;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import cookies.Database;

public class GeneralButton extends JButton {

	protected Database db;
	protected JTable searchResult;
	protected JTextField searchField;

	public GeneralButton(String name, Database db, JTable searchResult, JTextField searchField) {
		super(name);
		this.db = db;
		this.searchResult = searchResult;
		this.searchField = searchField;
	}

	protected void refreshGUI(String[] result) {
		DefaultTableModel tableModel = (DefaultTableModel) searchResult.getModel();
		int rows = tableModel.getRowCount();
		for (int i = rows - 1; i >= 0; --i) {
			tableModel.removeRow(i);
		}
		// tableModel.setColumnCount(8);
		for (int i = 0; i < result.length; i += 8) {
			String[] row = new String[8];
			row[0] = result[i];
			row[1] = result[i + 1];
			row[2] = result[i + 2];
			row[3] = result[i + 3];
			row[4] = result[i + 4];
			row[5] = result[i + 5];
			row[6] = result[i + 6];
			row[7] = result[i + 7];
			tableModel.addRow(row);
		}
		
		
		searchResult.repaint();
	}
	protected void showErrorMessage(String message){
		JOptionPane.showMessageDialog(searchResult, message);
	}
}
