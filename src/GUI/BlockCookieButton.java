package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import cookies.Database;

public class BlockCookieButton extends GeneralButton implements ActionListener {

	public BlockCookieButton(Database db, JTable searchResult, JTextField searchField) {
		super("Block/Unblock a certain cookie", db, searchResult, searchField);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String[] cookies = db.listCookies();
		Icon defaultIcon = null;
		String s = (String) JOptionPane.showInputDialog(searchResult, "Choose cookie to block", "Block cookie",
				JOptionPane.PLAIN_MESSAGE, defaultIcon, cookies, "cookie");
		if ((s != null) && (s.length() > 0)) {
			boolean blocked = db.isCookieBlocked(s);
			db.blockPalletsWithCookie(s, !blocked);
			db.setBlockedCookie(s, !blocked);
		}
	}
}
