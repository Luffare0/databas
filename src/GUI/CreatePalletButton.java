package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import cookies.Database;

public class CreatePalletButton extends GeneralButton implements ActionListener {

	private Database db;

	public CreatePalletButton(Database db, JTable searchResult, JTextField searchField) {
		super("Produce selected pallet(s)", db, searchResult, searchField);
		this.db = db;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int[] selectedRows = searchResult.getSelectedRows();
		TableModel model = searchResult.getModel();
		int couldNotProduce = 0;
		for (int i = 0; i < selectedRows.length; ++i) {
			int selectedRow = selectedRows[i];
			String status = (String) model.getValueAt(selectedRow, 2);
			if (status == "pre-production") {
				int palletId = Integer.parseInt((String) model.getValueAt(selectedRow, 0));
				boolean result = db.producePallet(palletId);
				if (result) {
					model.setValueAt("being produced", selectedRow, 2);
				} else {
					++couldNotProduce;
				}
			} else {
				++couldNotProduce;
			}
		}
		if (couldNotProduce > 0)
			showErrorMessage(
					couldNotProduce + " pallet" + (couldNotProduce > 1 ? "s" : "") + " could not be produced.");
	}

}
