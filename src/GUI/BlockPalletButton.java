package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import cookies.Database;

public class BlockPalletButton extends GeneralButton implements ActionListener {

	public BlockPalletButton(Database db, JTable searchResult, JTextField searchField) {
		super("Block/Unblock selected pallet(s)", db, searchResult, searchField);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int[] selectedRows = searchResult.getSelectedRows();
		TableModel model = searchResult.getModel();
		for (int i = 0; i < selectedRows.length; ++i) {
			int selectedRow = selectedRows[i];
			int palletId = Integer.parseInt((String) model.getValueAt(selectedRow, 0));
			String blocked = (String) model.getValueAt(selectedRow, 7);
			boolean result;
			result = db.blockPallet(palletId, blocked.equals("not blocked"));
			if (result) {
				model.setValueAt(blocked == "blocked" ? "not blocked" : "blocked", selectedRow, 7);
			} else {
				showErrorMessage("Could not block or unblock selected pallet");
			}
		}

	}

}
