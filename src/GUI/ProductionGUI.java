package GUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import se.datadosen.component.RiverLayout;
import cookies.Database;

import java.awt.*;

public class ProductionGUI {

	private Database db;

	public ProductionGUI(Database db) {
		this.db = db;

		JFrame f = new JFrame("Production");
		Container c = f.getContentPane();
		c.setLayout(new RiverLayout());
		JLabel connectionStatus = new JLabel();

		JTextField searchField = new JTextField();
		String[] columnNames = { "Pallet Id", "Cookie Type", "Status", "Current Location", "Order Id", "Time Produced",
				"Time Delivered", "Blocked" };
		JTable searchResult = new JTable();
		DefaultTableModel tableModel = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}
		};
		tableModel.setColumnIdentifiers(columnNames);
		searchResult.setModel(tableModel);
		searchResult.getColumnModel().getColumn(0).setPreferredWidth(55);
		searchResult.getColumnModel().getColumn(1).setPreferredWidth(103);
		searchResult.getColumnModel().getColumn(2).setPreferredWidth(80);
		searchResult.getColumnModel().getColumn(3).setPreferredWidth(162);
		searchResult.getColumnModel().getColumn(4).setPreferredWidth(68);
		searchResult.getColumnModel().getColumn(5).setPreferredWidth(155);
		searchResult.getColumnModel().getColumn(6).setPreferredWidth(155);
		searchResult.getColumnModel().getColumn(7).setPreferredWidth(105);
		searchResult.setPreferredScrollableViewportSize(searchResult.getPreferredSize());

		SearchButton searchButton = new SearchButton(db, searchResult, searchField);
		searchButton.addActionListener(searchButton);
		ListAllPalletsButton listAllPalletsButton = new ListAllPalletsButton(db, searchResult, searchField);
		listAllPalletsButton.addActionListener(listAllPalletsButton);
		ListBlockedPalletsButton listBlockedPalletsButton = new ListBlockedPalletsButton(db, searchResult, searchField);
		listBlockedPalletsButton.addActionListener(listBlockedPalletsButton);
		BlockPalletButton blockPalletButton = new BlockPalletButton(db, searchResult, searchField);
		blockPalletButton.addActionListener(blockPalletButton);
		CreatePalletButton createPalletButton = new CreatePalletButton(db, searchResult, searchField);
		createPalletButton.addActionListener(createPalletButton);
		SetPalletAsProducedButton setPalletAsProducedButton = new SetPalletAsProducedButton(db, searchResult,
				searchField);
		setPalletAsProducedButton.addActionListener(setPalletAsProducedButton);
		BlockCookieButton blockCookieButton = new BlockCookieButton(db, searchResult, searchField);
		blockCookieButton.addActionListener(blockCookieButton);
		SetPalletToBeingDelivered setPalletToBeingDelivered = new SetPalletToBeingDelivered(db, searchResult, searchField);
		setPalletToBeingDelivered.addActionListener(setPalletToBeingDelivered);
		SetPalletToDelivered setPalletToDelivered = new SetPalletToDelivered(db, searchResult, searchField);
		setPalletToDelivered.addActionListener(setPalletToDelivered);

		c.add("center", connectionStatus);
		c.add("p left", new JLabel("Enter Cookie name, order id, pallet id, status, date or date and time"));
		c.add("tab hfill", searchField);
		c.add("right", searchButton);
		c.add("br vtop", new JLabel("Search result"));
		JScrollPane jsp = new JScrollPane(searchResult);
		jsp.setPreferredSize(new Dimension(883, 400));
		c.add("tab hfill vfill", jsp);
		c.add("p left", createPalletButton);
		c.add("left", setPalletAsProducedButton);
		c.add("left", listAllPalletsButton);
		c.add("left", setPalletToBeingDelivered);
		c.add("br left", listBlockedPalletsButton);
		c.add("left", blockPalletButton);
		c.add("left", blockCookieButton);
		c.add("left", setPalletToDelivered);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack();
		f.setVisible(true);

		if (db.openConnection("db35", "dat12ole")) {
			connectionStatus.setText("Connected to database for Krusty Kookies Sweden AB");
		} else {
			connectionStatus.setText("Could not connect to database for Krusty Kookies Sweden AB");
		}

	}

	public static void main(String[] args) {
		Database db = new Database();
		new ProductionGUI(db);
	}
}
