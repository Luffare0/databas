package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import cookies.Database;

public class SetPalletToDelivered extends GeneralButton implements ActionListener{

	public SetPalletToDelivered(Database db, JTable searchResult,
			JTextField searchField) {
		super("Set selected pallet(s) to delivered", db, searchResult, searchField);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int[] selectedRows = searchResult.getSelectedRows();
		TableModel model = searchResult.getModel();
		for (int i = 0; i < selectedRows.length; ++i) {
			int selectedRow = selectedRows[i];
			int palletId = Integer.parseInt((String) model.getValueAt(selectedRow, 0));
			boolean result = db.setPalletToDelivered(palletId);
			if (result) {
				model.setValueAt("delivered", selectedRow, 2);
			} else {
				showErrorMessage("Could not set to delivered");
			}
		}
		
	}
		
	}


