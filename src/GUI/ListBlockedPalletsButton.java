package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.JTextField;

import cookies.Database;

public class ListBlockedPalletsButton extends GeneralButton implements ActionListener {


	public ListBlockedPalletsButton(Database db, JTable searchResult, JTextField searchField){
		super("List blocked pallets", db, searchResult, searchField);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String[] result = db.listBlockedPallets();
		refreshGUI(result);
	}

}
