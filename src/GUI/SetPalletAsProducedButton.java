package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import cookies.Database;

public class SetPalletAsProducedButton extends GeneralButton implements ActionListener {

	public SetPalletAsProducedButton(Database db, JTable searchResult, JTextField searchField) {
		super("Set selected pallets as produced", db, searchResult, searchField);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int selectedRow = searchResult.getSelectedRow();
		TableModel model = searchResult.getModel();
		if (selectedRow != -1) {
			String status = (String) model.getValueAt(selectedRow, 2);
			if (status == "being produced") {
				int palletId = Integer.parseInt((String) model.getValueAt(selectedRow, 0));
				boolean result = db.setStatusToProduced(palletId);
				if (result) {
					model.setValueAt("produced", selectedRow, 2);
				} else {
					showErrorMessage("Could not set as produced");
				}
			} else {
				showErrorMessage("Can't set a cookie in pre-production to produced");
			}
		}
	}

}
