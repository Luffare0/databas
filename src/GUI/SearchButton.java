package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.*;

import javax.swing.JTable;
import javax.swing.JTextField;
import cookies.Database;

public class SearchButton extends GeneralButton implements ActionListener {

	public SearchButton(Database db, JTable searchResult, JTextField searchField) {
		super("Search", db, searchResult, searchField);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String text = searchField.getText();
		ArrayList<String> searchedPallet = new ArrayList<String>();
		try {
			for (String s : db.listPalletFromId(Integer.parseInt(text)))
				searchedPallet.add(s);
			for (String s : db.listPalletsWithOrderId(Integer.parseInt(text)))
				searchedPallet.add(s);
		} catch (NumberFormatException exc) {
			// it wasn't a number
		}
		for (String s : db.listPalletFromStatus(db.getStatusFromString(text)))
			searchedPallet.add(s);
		for (String s : db.listPalletFromCookie(text))
			searchedPallet.add(s);
		String[] times = text.split(" ");

		if (checkIfDate(times[0])) {
			String time1, time2 = "9999-12-31 23:59:59";
			time1 = times[0];
			for (int i = 1; i < times.length; ++i) {
				if (checkIfTime(times[i]) && time1.length() < 11 && time2.length() > 11)
					time1 += " " + times[i];
				else if (checkIfDate(times[i]))
					time2 = times[i];
				else if (checkIfTime(times[i]) && time2.length() < 11)
					time2 += " " + times[i];
			}

			if (time1.length() < 11)
				time1 += " 00:00:00";
			if (time2.length() < 11)
				time2 += " 23:59:59";
			// System.out.println(time1 + " " + time2);
			for (String s : db.listPalletProducedBetween(time1, time2))
				searchedPallet.add(s);
		}
		refreshGUI(searchedPallet.toArray(new String[searchedPallet.size()]));
	}

	public boolean checkIfDate(String s) {
		if (s.length() == 10)
			return Pattern.matches("[0-9]{4}[^0-9][0-9]{2}[^0-9][0-9]{2}", s);
		return false;
	}

	public boolean checkIfTime(String s) {
		if (s.length() == 8)
			return Pattern.matches("[0-9]{2}[^0-9][0-9]{2}[^0-9][0-9]{2}", s);
		return false;
	}
}
