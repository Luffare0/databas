package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.JTextField;

import cookies.Database;

public class ListAllPalletsButton extends GeneralButton implements ActionListener {

	public ListAllPalletsButton(Database db, JTable searchResult, JTextField searchField) {
		super("List All Pallets", db, searchResult, searchField);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String[] result = db.listPallets();
		refreshGUI(result);
	}
}
