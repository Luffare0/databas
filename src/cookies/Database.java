package cookies;

import java.sql.*;
import java.util.ArrayList;

public class Database {

	public static final int PALLET_STATUS_PRE_PRODUCTION = 0;
	public static final int PALLET_STATUS_BEING_PRODUCED = 1;
	public static final int PALLET_STATUS_PRODUCED = 2;
	public static final int PALLET_STATUS_BEING_DELIVERED = 3;
	public static final int PALLET_STATUS_DELIVERED = 4;

	public static final int PALLET_NOT_BLOCKED = 0;
	public static final int PALLET_BLOCKED = 1;

	private Connection conn;

	/**
	 * Creates a new object that can be used to connect to and interact with the
	 * database for cookies.
	 */

	public Database() {
		conn = null;
	}

	/**
	 * Open a new connection to the sql database.
	 * 
	 * @param userName
	 *            A valid username
	 * @param password
	 *            A valid password
	 * @return True if the login was successful, false otherwise
	 */

	public boolean openConnection(String userName, String password) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(
					"jdbc:mysql://puccini.cs.lth.se/" + userName + "?zeroDateTimeBehavior=convertToNull", userName,
					password);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Close the connection to the sql database.
	 */

	public void closeConnection() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
		}
		conn = null;
	}

	/**
	 * Check if connected to the database
	 * 
	 * @return True if connected to the database, False otherwise
	 */

	public boolean isConnected() {
		return conn != null;
	}

	public boolean login(String userId) {
		// attempts to login with the specified username
		try {
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery("select name from Users where name = \'" + userId + "\'");
			return result.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Insert a new cookie into the database.
	 * 
	 * @param name
	 *            The name of the cookie
	 * @return True if the insertion was successful, false otherwise
	 */

	public boolean createNewCookie(String name) {
		try {
			String stmt = "insert into Cookies values ( ? , 0 );";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, name);
			prStmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * Insert a new material into the database.
	 * 
	 * @param name
	 *            The name of the material
	 * @param unit
	 *            A string describing the unit this material is measured in
	 * @return True if the insertion was successful, false otherwise
	 */

	public boolean createNewMaterial(String name, String unit) {
		try {
			String stmt = "insert into RawMaterials (Name, Quantity, Unit) values ( ? , 0 , ? );";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, name);
			prStmt.setString(2, unit);
			prStmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	/**
	 * Update the database by adding an amount of a material to the storage.
	 * 
	 * @param name
	 *            The material to be added
	 * @param amount
	 *            The amount to add
	 * @return True if the update was successful, false otherwise
	 */
	public boolean updateMaterial(String name, int amount) {
		try {
			String stmt = "select Quantity from RawMaterials where name = ? for update;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, name);
			ResultSet result = prStmt.executeQuery();
			result.next();
			int currentAmount = Integer.parseInt(result.getString(1));

			stmt = "update RawMaterials set Quantity = ?, LastTransportQuantity = ?  where Name = ?;";
			prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, "" + (currentAmount + amount));
			prStmt.setString(2, "" + amount);
			prStmt.setString(3, name);
			prStmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			// System.out.println(e.getMessage());
			return false;
		}
	}

	/**
	 * Add a new ingredient to the database.
	 * 
	 * @param recipeName
	 *            The name of the cookie
	 * @param materialName
	 *            The name of the material
	 * @param amount
	 *            The amount needed in the recipe
	 * @return True if the insertion was successful, false otherwise
	 */

	public boolean createNewIngredient(String recipeName, String materialName, int amount) {
		try {
			String stmt = "insert into Ingredients values ( ? , ? , ? );";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, materialName);
			prStmt.setString(2, recipeName);
			prStmt.setString(3, "" + amount);
			prStmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * List all ingredients and their amounts for a cookie.
	 * 
	 * @param cookieName
	 *            The name of the cookie
	 * @return An array of Strings containing {materialName1, amount1,
	 *         materialName2, amount2, ... }
	 */

	public String[] listIngredients(String cookieName) {
		try {
			String stmt = "select MaterialName, Amount from Ingredients where RecipeName = ?;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, cookieName);
			ResultSet result = prStmt.executeQuery();
			ArrayList<String> ingredients = new ArrayList<String>();
			while (result.next()) {
				ingredients.add(result.getString(1));
				ingredients.add(result.getString(2));
			}
			return ingredients.toArray(new String[ingredients.size()]);
		} catch (SQLException e) {
			e.getMessage();
			return new String[0];
		}
	}

	/**
	 * List all the cookies in the database.
	 * 
	 * @return A array of String containing the names of the cookies
	 */

	public String[] listCookies() {
		try {
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery("select Name from Cookies;");
			ArrayList<String> recipes = new ArrayList<String>();
			while (result.next()) {
				recipes.add(result.getString(1));
			}
			return recipes.toArray(new String[recipes.size()]);
		} catch (SQLException e) {
			return new String[0];
		}
	}

	/**
	 * Check if a cookie is blocked
	 * 
	 * @param cookieName
	 *            The name of the cookie
	 * @return True if the cookie is blocked, false otherwise
	 */

	public boolean isCookieBlocked(String cookieName) {
		try {
			String stmt = "select IsBlocked from Cookies where Name = ?;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, cookieName);
			ResultSet result = prStmt.executeQuery();
			result.next();
			return result.getString(1).equals("1");
		} catch (SQLException e) {
			// System.out.println(e.getMessage());
			return false;
		}
	}

	/**
	 * Set a cookie to blocked or unblocked.
	 * 
	 * @param cookieName
	 *            The name
	 * @param block
	 *            True if this pallet should be set to blocked, false if it
	 *            should be set to unblocked
	 * @return True if the update was successful, false otherwise
	 */

	public boolean setBlockedCookie(String cookieName, boolean block) {

		try {
			String stmt = "update Cookies set IsBlocked = b'" + (block ? "1" : "0") + "' where Name = ?;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, cookieName);
			return prStmt.executeUpdate() == 1;
		} catch (SQLException e) {
			// System.out.println(e.getMessage());
			return false;
		}
	}

	/**
	 * List all the materials and their amounts in the database.
	 * 
	 * @return An array of Strings containing {materialName1, amount1,
	 *         materialName2, amount2, ... }
	 */

	public String[] listMaterials() {
		try {
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery("select Name, Quantity from RawMaterials;");
			ArrayList<String> r = new ArrayList<String>();
			while (result.next()) {
				r.add(result.getString(1));
				r.add(result.getString(2));
			}
			return r.toArray(new String[r.size()]);
		} catch (SQLException e) {
			// System.out.println(e.getMessage());
			return new String[0];
		}
	}

	/**
	 * Change the status of a pallet to PALLET_STATUS_BEING_PRODUCED, if there
	 * are enough raw materials to produce it, and withdraw the correct amount
	 * of raw material from the database
	 * 
	 * @param id
	 *            The pallet's id
	 * @return True if the status update was successful, false otherwise
	 */

	public boolean producePallet(int id) {
		try {
			Statement stmt = conn.createStatement();
			conn.setAutoCommit(false);
			stmt.execute("start transaction");
			ResultSet result;
			PreparedStatement prStmt = conn.prepareStatement("select CookieName from Pallets where Id = ? ;");
			prStmt.setInt(1, id);
			result = prStmt.executeQuery();
			if (!result.next())
				return false;
			String cookieName = result.getString(1);
			String[] ingredients = listIngredients(cookieName);
			// check if we have all the needed materials
			for (int i = 0; i < ingredients.length - 1; i++) {
				// each pallet holds 5400 cookies and each recipe is for 100
				// cookies, hence * 54
				String materialName = ingredients[i];
				int amountNeeded = Integer.parseInt(ingredients[++i]) * 54;
				result = stmt.executeQuery("select Quantity from RawMaterials where Name = \"" + materialName + "\";");
				if (!(result.next() && Integer.parseInt(result.getString(1)) > amountNeeded)) {
					conn.setAutoCommit(true);
					return false;
				}
			}
			// if we have all the required materials, then we can subtract that
			// much from the storage (and bake delicious cookies)
			for (int i = 0; i < ingredients.length - 1; i++) {
				int amountNeeded = Integer.parseInt(ingredients[++i]) * 54;
				stmt.executeUpdate("update RawMaterials set Quantity =  Quantity - " + amountNeeded + " where Name = \""
						+ ingredients[i - 1] + "\";");
			}

			prStmt = conn.prepareStatement(
					"update Pallets set Status = " + PALLET_STATUS_BEING_PRODUCED + " where Id = ? ;");
			prStmt.setInt(1, id);
			boolean itWorked = false;
			if (prStmt.executeUpdate() > 0)
				itWorked = true;
			conn.commit();
			conn.setAutoCommit(true);
			return itWorked;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Set a pallet to PALLET_STATUS_BEING_DELIVERED, if it is
	 * PALLET_STATUS_PRODUCED
	 * 
	 * @param palletId
	 *            The pallet's id
	 * @return True if the update was successful, false otherwise
	 */

	public boolean setPalletToBeingDelivered(int palletId) {
		try {
			conn.setAutoCommit(false);
			String stmt = "select Status from Pallets where Id = ? for update;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setInt(1, palletId);
			ResultSet result = prStmt.executeQuery();
			if (!result.next() || result.getInt(1) != PALLET_STATUS_PRODUCED) {
				conn.setAutoCommit(true);
				return false;
			}
			stmt = "update Pallets set Status = ? where Id = ?;";
			prStmt = conn.prepareStatement(stmt);
			prStmt.setInt(1, PALLET_STATUS_BEING_DELIVERED);
			prStmt.setInt(2, palletId);
			boolean itWorked = false;
			if (prStmt.executeUpdate() > 0) {
				itWorked = true;
			}
			conn.setAutoCommit(true);
			return itWorked;
		} catch (SQLException e) {
			e.getMessage();
			return false;
		}
	}

	/**
	 * Set a pallet to PALLET_STATUS_DELIVERED, if it is
	 * PALLET_STATUS_BEING_DELIVERED
	 * 
	 * @param palletId
	 *            The pallet's id
	 * @return True if the update was successful, false otherwise
	 */

	public boolean setPalletToDelivered(int palletId) {
		try {
			conn.setAutoCommit(false);
			String stmt = "select Status from Pallets where Id = ? for update;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setInt(1, palletId);
			ResultSet result = prStmt.executeQuery();
			if (!result.next() || result.getInt(1) != PALLET_STATUS_BEING_DELIVERED) {
				conn.setAutoCommit(true);
				return false;
			}
			stmt = "update Pallets set Status = ?, DeliveryDateTime = now() where Id = ?;";
			prStmt = conn.prepareStatement(stmt);
			prStmt.setInt(1, PALLET_STATUS_DELIVERED);
			prStmt.setInt(2, palletId);
			boolean itWorked = false;
			if (prStmt.executeUpdate() > 0) {
				itWorked = true;
			}
			conn.setAutoCommit(true);
			return itWorked;
		} catch (SQLException e) {

			return false;
		}
	}

	/**
	 * Add a new pallet of cookies to the database.
	 * 
	 * @param cookieName
	 *            The name of the cookies on the pallet
	 * @param orderId
	 *            The id of the order that this pallet belongs to
	 * @return True if insertion was successful, false if there is not enough
	 *         raw materials to produce this pallet or otherwise
	 */
	public boolean newPallet(String cookieName, int orderId) {
		try {
			String stmt = "insert into pallets (cookiename, status, currentlocation, orderid, deliverydatetime,isblocked) values ( ? , "
					+ PALLET_STATUS_PRE_PRODUCTION + ", \"the factory\", ? , 0 , b'" + PALLET_NOT_BLOCKED + "')";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, cookieName);
			prStmt.setInt(2, orderId);
			if (prStmt.executeUpdate() > 0)
				return true;
			return false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}

	}

	/**
	 * Change the status of a pallet to PALLET_STATUS_PRODUCED, if it's
	 * PALLET_STATUS_BEING_PRODUCED.
	 * 
	 * @param palletId
	 *            The pallet's id
	 * @return True if the update was successful, false otherwise
	 */

	public boolean setStatusToProduced(int palletId) {
		try {
			String stmt = "update Pallets set Status = " + PALLET_STATUS_PRODUCED + " where (Id, Status) = (?, ?);";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setInt(1, palletId);
			prStmt.setInt(2, PALLET_STATUS_BEING_PRODUCED);
			if (prStmt.executeUpdate() > 0)
				return true;
			return false;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * Converts the ResultSet from an SQL SELECT statement from the Pallets
	 * table to a String[]
	 * 
	 * @param result
	 *            The ResultSet to be converted
	 * @return A String[] with the contents of the ResultSet
	 */

	private String[] palletQueryResults(ResultSet result) {
		ArrayList<String> s = new ArrayList<String>();
		try {
			while (result.next()) {
				s.add(result.getString(1));
				s.add(result.getString(2));
				s.add(getStatusString(Integer.parseInt(result.getString(3))));
				s.add(result.getString(4));
				s.add(result.getString(5));
				s.add(result.getString(6));
				if (result.getTimestamp(7) == null)
					s.add("0000-00-00 00:00:00");
				else
					s.add(result.getTimestamp(7).toString());
				s.add(getBlockedString(Integer.parseInt(result.getString(8))));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return s.toArray(new String[s.size()]);

	}

	/**
	 * List all the pallets.
	 * 
	 * @return An array of Strings containing {palletId, cookieName, status,
	 *         currentLocation, orderId, productionDateTime, DeliveryDateTime,
	 *         isBocked, ... }
	 */

	public String[] listPallets() {

		try {
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery("select * from Pallets;");

			return palletQueryResults(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return new String[0];
	}

	/**
	 * List all pallets that are blocked.
	 * 
	 * @return An array of Strings containing {palletId, cookieName, status,
	 *         currentLocation, orderId, productionDateTime, DeliveryDateTime,
	 *         isBocked, ... }
	 */

	public String[] listBlockedPallets() {
		try {
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery("select * from Pallets where IsBlocked = b'" + PALLET_BLOCKED + "';");
			return palletQueryResults(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new String[0];
	}

	/**
	 * List all info of pallets containing a specific cookie type.
	 * 
	 * @param cookieName
	 *            The cookie that the pallets
	 * @return An array of Strings containing {palletId, cookieName, status,
	 *         currentLocation, orderId, productionDateTime, DeliveryDateTime,
	 *         isBocked} for all listed pallets
	 */

	public String[] listPalletFromCookie(String cookieName) {
		try {
			String stmt = "select * from Pallets where cookieName = ?;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, cookieName);
			ResultSet result = prStmt.executeQuery();
			return palletQueryResults(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new String[0];
	}

	/**
	 * List all info of a pallet with a specific id.
	 * 
	 * @param id
	 *            The id of the pallet
	 * @return An array of Strings containing {palletId, cookieName, status,
	 *         currentLocation, orderId, productionDateTime, DeliveryDateTime,
	 *         isBocked} for all listed pallets
	 */

	public String[] listPalletFromId(int id) {
		try {
			String stmt = "select * from Pallets where id = ?;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, "" + id);
			ResultSet result = prStmt.executeQuery();
			return palletQueryResults(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new String[0];
	}

	/**
	 * List all pallet that have a certain status. Possible statuses are:
	 * PALLET_STATUS_PRE_PRODUCTION, PALLET_STATUS_BEING_PRODUCED
	 * PALLET_STATUS_PRODUCED, PALLET_STATUS_DELIVERED, PALLET_STATUS_ON_THE_WAY
	 * 
	 * @param status
	 *            The status to filter for
	 * @return An array of Strings containing {palletId, cookieName, status,
	 *         currentLocation, orderId, productionDateTime, DeliveryDateTime,
	 *         isBocked} for all listed pallets
	 */

	public String[] listPalletFromStatus(int status) {
		try {
			String stmt = "select * from Pallets where status = ?;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, "" + status);
			ResultSet result = prStmt.executeQuery();
			return palletQueryResults(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new String[0];
	}

	/**
	 * Block or unblock a pallet.
	 * 
	 * @param id
	 *            The pallet's id
	 * @param blocked
	 *            PALLET_BLOCKED or PALLET_NOT_BLOCKED
	 * @return True if the update was successful, false otherwise
	 */

	public boolean blockPallet(int id, boolean block) {
		try {
			String stmt = "update Pallets set IsBlocked = ? where id = ? ;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setInt(1, block ? PALLET_BLOCKED : PALLET_NOT_BLOCKED);
			prStmt.setString(2, "" + id);
			prStmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Block all pallets that contain a certain type of cookie.
	 * 
	 * @param cookieName
	 *            The name of the cookie
	 * @return True if the update was successful, false otherwise
	 */

	public boolean blockPalletsWithCookie(String cookieName, boolean block) {
		try {
			String stmt = "update Pallets set IsBlocked = b'" + (block ? "1" : "0") + "' where cookieName = ? ;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setInt(1, block ? PALLET_BLOCKED : PALLET_NOT_BLOCKED);
			prStmt.setString(2, cookieName);
			prStmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * List all pallets that were produced between two dates. The dates should
	 * be on the form "yyyy-mm-dd hh:mm:ss"
	 * 
	 * @param time1
	 *            The minimum time a pallet may have been produced
	 * @param time2
	 *            The maximum time a pallet may have been produced
	 * @return An array of Strings containing {palletId, cookieName, status,
	 *         currentLocation, orderId, productionDateTime, DeliveryDateTime,
	 *         isBocked} for all listed pallets
	 */

	public String[] listPalletProducedBetween(String time1, String time2) {
		try {
			String stmt = "select * from Pallets where ProductionDateTime between ? and  ?;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, time1);
			prStmt.setString(2, time2);
			ResultSet result = prStmt.executeQuery();
			return palletQueryResults(result);
		} catch (SQLException e) {
			e.printStackTrace();
			return new String[0];
		}
	}

	/**
	 * Get the current location of a pallet.
	 * 
	 * @param id
	 *            The pallet's id
	 * @return A string containing the pallet's current location
	 */

	public String locatePallet(int id) {
		try {
			String stmt = "select currentLocation from Pallets where id = ?;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, "" + id);
			ResultSet result = prStmt.executeQuery();
			result.next();
			return result.getString(1);
		} catch (SQLException e) {
			return "";
		}
	}

	/**
	 * Add a new customer to the database.
	 * 
	 * @param name
	 *            The customer's name
	 * @param address
	 *            The customer's address
	 * @return True if the insertion was successful, false otherwise
	 */

	public boolean newCustomer(String name, String address) {
		try {
			String stmt = "insert into Customers values ( ? , ? );";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, name);
			prStmt.setString(2, address);
			if (prStmt.executeUpdate() > 0)
				return true;
			else
				return false;
		} catch (SQLException e) {
			// System.out.println(e.getMessage());
			return false;
		}
	}

	/**
	 * List all the customers in the database.
	 * 
	 * @return An array of Strings containing {name1, address1, name2, address2,
	 *         ...}
	 */

	public String[] listCustomers() {
		try {
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery("select * from Customers;");
			ArrayList<String> r = new ArrayList<String>();
			while (result.next()) {
				r.add(result.getString(1));
				r.add(result.getString(2));
			}
			return r.toArray(new String[r.size()]);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new String[0];
	}

	/**
	 * Place a new order.
	 * 
	 * @param customerName
	 *            The customer's name
	 * @param destination
	 *            The customer's address
	 * @param cookieName
	 *            The name of the cookie
	 * @param nPallets
	 *            The number of pallets
	 * @param deliveryDate
	 *            The date the pallets are to be delivered
	 * @return True if the insertion was successful, false otherwise (such as if
	 *         customer or order isn't in the database)
	 */

	public boolean placeOrder(String customerName, String destination, String cookieName, int nPallets,
			String deliveryDate) {

		try {
			String stmt = "insert into Orders (Destination, DeliveryDate, CustomerName) values ( ? , ? , ? );";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setString(1, destination);
			prStmt.setString(2, deliveryDate);
			prStmt.setString(3, customerName);
			if (prStmt.executeUpdate() < 1)
				return false;
			Statement stmt2 = conn.createStatement();
			ResultSet result = stmt2.executeQuery("select last_insert_id();");
			if (!result.next())
				return false;
			int orderId = Integer.parseInt(result.getString(1));
			for (int i = 0; i < nPallets; ++i) {
				if (!newPallet(cookieName, orderId))
					return false;
			}
			return true;
		} catch (

		SQLException e)

		{
			System.out.println(e.getMessage());
			return false;
		}

	}

	/**
	 * Convert a ResultSet to a String[] from a SQL SELECT statment on the
	 * Orders table.
	 * 
	 * @param result
	 *            The ResultSet
	 * @return A String[] with the contents of the ResultSet
	 */

	private String[] ordersQueryResults(ResultSet result) {
		ArrayList<String> r = new ArrayList<String>();
		try {
			while (result.next()) {
				r.add(result.getString(1));
				r.add(result.getString(2));
				if (result.getTimestamp(3) == null)
					r.add("0000-00-00 00:00:00");
				else
					r.add(result.getTimestamp(3).toString());
				if (result.getTimestamp(4) == null)
					r.add("0000-00-00 00:00:00");
				else
					r.add(result.getTimestamp(4).toString());
				r.add(result.getString(5));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return r.toArray(new String[r.size()]);

	}

	/**
	 * List all the orders.
	 * 
	 * @return An array of Strings containing {orderId, destination,
	 *         deliveryDate, timeOrdered, customerName, ...}
	 */

	public String[] listOrders() {
		try {
			Statement stmt = conn.createStatement();
			ResultSet result = stmt.executeQuery("select * from Orders;");
			return ordersQueryResults(result);
		} catch (SQLException e) {
			return new String[0];
		}
	}

	/**
	 * List all info of an order with a specific id.
	 * 
	 * @param id
	 *            The order's id
	 * @return A an array of Strings containing {orderId, destination,
	 *         deliveryDate, timeOrdered, customerName, ...}
	 */

	public String[] listPalletsWithOrderId(int id) {
		try {
			String stmt = "select * from Pallets where OrderId = ?;";
			PreparedStatement prStmt = conn.prepareStatement(stmt);
			prStmt.setInt(1, id);
			ResultSet result = prStmt.executeQuery();
			return palletQueryResults(result);
		} catch (SQLException e) {
			return new String[0];
		}
	}

	/**
	 * Get a status from a String.
	 * 
	 * @param s
	 *            The String describing the status
	 * @return An int describing the status, -1 if a String that does not match
	 *         a status is given
	 */

	public int getStatusFromString(String s) {
		switch (s) {
		case "pre-production":
			return PALLET_STATUS_PRE_PRODUCTION;
		case "being produced":
			return PALLET_STATUS_BEING_PRODUCED;
		case "produced":
			return PALLET_STATUS_PRODUCED;
		case "delivered":
			return PALLET_STATUS_DELIVERED;
		case "being delivered":
			return PALLET_STATUS_BEING_DELIVERED;
		}
		return -1; // not a status
	}

	// PALLET_STATUS_PRE_PRODUCTION = 0;
	// PALLET_STATUS_BEING_PRODUCED = 1;
	// PALLET_STATUS_PRODUCED = 2;
	// PALLET_STATUS_ON_THE_WAY = 3;
	// PALLET_STATUS_DELIVERED = 4;

	/**
	 * Get a String describing a status
	 * 
	 * @param status
	 *            The status to be described
	 * @return A descriptive String
	 */

	public String getStatusString(int status) {
		switch (status) {
		case PALLET_STATUS_PRE_PRODUCTION:
			return "pre-production";
		case PALLET_STATUS_BEING_PRODUCED:
			return "being produced";
		case PALLET_STATUS_PRODUCED:
			return "produced";
		case PALLET_STATUS_DELIVERED:
			return "delivered";
		case PALLET_STATUS_BEING_DELIVERED:
			return "being delivered";
		}
		return "invalid argument";
	}

	private String getBlockedString(int i) {
		switch (i) {
		case PALLET_BLOCKED:
			return "blocked";
		case PALLET_NOT_BLOCKED:
			return "not blocked";
		}
		return "invalid argument";
	}

}
