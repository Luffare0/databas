package cookies;

import java.util.Scanner;

public class TestProduction {

	public static void printPalletInfo(String[] s) {
		if (s.length == 0) {
			System.out.println("No pallet(s) found");
		}
		for (int i = 0; i < s.length; i++) {
			System.out.print(s[i] + " ");
			System.out.print(s[++i] + " ");
			System.out.print(s[++i] + " ");
			System.out.print(s[++i] + " ");
			System.out.print(s[++i] + " ");
			System.out.print(s[++i] + " ");
			System.out.print(s[++i] + " ");
			System.out.println(s[++i]);
		}
	}

	public static void main(String[] args) {
		Database db = new Database();
		db.openConnection("db35", "dat12ole");
		String input;
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Test program for production\nCommands:\nnewPallet [cookie name] [order id]\nlistPallets\nlistBlockedPallets\nlistPalletsFromId [pallet id]\nlistPalletFromStatus [status (int)]\nblockPallet [pallet id] [block/unblock]\nblockPalletsWithCookie [cookie name] [block/unblock]\nlocatePallet [pallet id]\n");
		while (scanner.hasNext()) {
			input = scanner.nextLine();
			String[] commands = input.split(" ");
			switch (commands[0]) {
			case "newPallet":
				if (db.newPallet(commands[1], Integer.parseInt(commands[2])))
					System.out.println("Pallet ordered");
				else
					System.out.println("Pallet could not be sent");
				break;
			case "listPallets":
				String[] r = db.listPallets();
				printPalletInfo(r);
				break;
			case "listBlockedPallets":
				r = db.listBlockedPallets();
				printPalletInfo(r);
				break;

			case "listPalletFromId":
				r = db.listPalletFromId(Integer.parseInt(commands[1]));
				printPalletInfo(r);
				break;

			case "listPalletsFromStatus":
				r = db.listPalletFromStatus(Integer.parseInt(commands[1]));
				printPalletInfo(r);
				break;
			case "blockPallet":
				if (db.blockPallet(Integer.parseInt(commands[1]), commands[2].equals("block")))
					System.out.println("Pallet blocked / unblocked");
				else
					System.out.println("Could not block/unblock pallet");
				break;
			case "blockPalletsWithCookie":
				if (db.blockPalletsWithCookie(commands[1], commands[2].equals("block")))
					System.out.println("Pallet(s) blocked");
				else
					System.out.println("Pallet(s) could not be blocked");
				break;
			case "locatePallet":
				System.out.println(db.locatePallet(Integer.parseInt(commands[1])));
				break;
			default:
				System.out.println("Not a command");
			}
		}
		scanner.close();
	}

}
