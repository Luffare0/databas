package cookies;

import java.util.Scanner;

public class TestOrders {

	public static void main(String[] args) {
		Database db = new Database();
		db.openConnection("db35", "dat12ole");
		String input;
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Test program for orders\nCommands:\nnewCustomer [name] [address]\nlistCustomers\nplaceOrder [customer name] [customer address] [cookie name] [number of pallets] [delivery time]\nlistOrders\naddToOrder [order id] [cookie name] [number of pallets]");
		while (scanner.hasNext()) {
			input = scanner.nextLine();
			String[] commands = input.split(" ");
			switch (commands[0]) {
			case "newCustomer":
				if (db.newCustomer(commands[1], commands[2]))
					System.out.println("Customer added");
				else
					System.out.println("Customer could not be added");
				break;
			case "listCustomers":
				String[] r = db.listCustomers();
				for (int i = 0; i < r.length - 1; i++) {
					System.out.print(r[i] + " ");
					System.out.println(r[++i]);
				}
				break;
			case "placeOrder":
				if (db.placeOrder(commands[1], commands[2], commands[3], Integer.parseInt(commands[4]), commands[5]))
					System.out.println("Order placed, please wait for your cookies");
				else
					System.out.println("Order could not be placed");
				break;
			case "listOrders":
				r = db.listOrders();
				for (int i = 0; i < r.length - 1; i++) {
					System.out.print(r[i] + " ");
					System.out.print(r[++i] + " ");
					System.out.print(r[++i] + " ");
					System.out.print(r[++i] + " ");
					System.out.println(r[++i]);
				}
				break;
			case "addToOrder":
				for (int i = 0; i < Integer.parseInt(commands[3]); ++i)
					if (db.newPallet(commands[2], Integer.parseInt(commands[1])))
						System.out.println("Pallet added to order");
					else
						System.out.println("Could not add order");
				break;
			default:
				System.out.println("Not a command");

			}
		}
		scanner.close();
	}

}
