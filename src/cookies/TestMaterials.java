package cookies;

import java.util.Scanner;

public class TestMaterials {

	public static void main(String[] args) {

		Database db = new Database();
		db.openConnection("db35", "dat12ole");
		String input;
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Test program for raw materials, ingredients and recipes. Commands:\nnewCookie [name]\nnewMaterial [name] [unit]\nupdateMaterial [name] [amount to add]\nnewIngredient [name of cookie] [name of material] [amount]\nlistIngredients [cookie name]\nlistCookies\nlistMaterials\n");
		while (scanner.hasNext()) {
			input = scanner.nextLine();
			String[] commands = input.split(" ");
			switch (commands[0]) {
			case "newCookie":
				if (db.createNewCookie(commands[1]))
					System.out.println("new cookie " + commands[1] + " added");
				else
					System.out.println("could not add cookie" + commands[1]);
				break;
			case "newMaterial":
				if (db.createNewMaterial(commands[1], commands[2]))
					System.out.println("new material " + commands[1] + " added");
				else
					System.out.println("could not add material " + commands[1]);
				break;
			case "updateMaterial":
				if (db.updateMaterial(commands[1], Integer.parseInt(commands[2])))
					System.out.println("material " + commands[1] + " updated");
				else
					System.out.println("could not update material " + commands[1]);
				break;
			case "newIngredient":
				if (db.createNewIngredient(commands[1], commands[2], Integer.parseInt(commands[3])))
					System.out.println("ingredient " + commands[2] + " for " + commands[1] + " added");
				else
					System.out.println("could not add ingredient " + commands[2] + " for " + commands[1]);
				break;
			case "listIngredients":
				String[] r = db.listIngredients(commands[1]);
				for (int i = 0; i < r.length - 1; i++) {
					System.out.print(r[i] + " ");
					System.out.println(r[++i]);
				}
				System.out.println();
				break;
			case "listCookies":
				r = db.listCookies();
				for (String s : r)
					System.out.println(s);
				break;
			case "listMaterials":
				r = db.listMaterials();
				for (int i = 0; i < r.length - 1; i++) {
					System.out.print(r[i] + " ");
					System.out.println(r[++i]);
				}
				System.out.println();
				break;
			default:
				System.out.println("That's not a command");
			}
		}
		scanner.close();
	}

}
